import numpy as np
from PIL import ImageGrab
import cv2
import time

def screen_record():
    last_time = time.time()
    while(True):
        printscreen = np.array(ImageGrab.grab(bbox=(0,40,800,640)))
        print('loop took () seconds'.format(time.time()-last_time))
        cv2.imshow('windows',cv2.cvtColor(printscreen, cv2.COLOR_BGR2RGB))
        if cv2.waitKey(25) & 0xFF == ord('q'):
            cv2.destroyAllWindows()
            break

screen_record()
